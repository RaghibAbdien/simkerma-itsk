<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reset Password</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <div class="container reset">
        <div class="reset column-left">
            <div class="side-bar">
                <img src="assets/images/amico1.png" alt="lupa password">
            </div>
        </div>
        <div class="reset column-right">
            <h1>Lupa Kata Sandi?</h1>
            <label for="password1">Kata Sandi Baru</label>
            <div class="input-group">
                <input type="password" class="form-control" id="password1" name="password" placeholder="Masukkan kata sandi" required>
                <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="fas fa-eye-slash" id="togglePassword1"></i>
                  </span>
                </div>
            </div>

            <label for="password2">Konfirmasi Kata Sandi</label>
            <div class="input-group">
                <input type="password" class="form-control" id="password2" name="password" placeholder="Masukkan kata sandi" required>
                <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="fas fa-eye-slash" id="togglePassword2"></i>
                  </span>
                </div>
            </div>

            <div class="button reset-button">
                <button type="submit">Kirim</button>
            </div>
        </div>
    </div>

    <script>
    const togglePassword1 = document.querySelector('#togglePassword1');
    const password1 = document.querySelector('#password1');

        togglePassword1.addEventListener('click', function() {
            if (password1.type === 'password') {
                password1.type = 'text';
                togglePassword1.classList.add('fa-eye');
                togglePassword1.classList.remove('fa-eye-slash');
            } else {
                password1.type = 'password';
                togglePassword1.classList.add('fa-eye-slash');
                togglePassword1.classList.remove('fa-eye');
            }
        });

        const togglePassword2 = document.querySelector('#togglePassword2');
        const password2 = document.querySelector('#password2');

        togglePassword2.addEventListener('click', function() {
            if (password2.type === 'password') {
                password2.type = 'text';
                togglePassword2.classList.add('fa-eye');
                togglePassword2.classList.remove('fa-eye-slash');
            } else {
                password2.type = 'password';
                togglePassword2.classList.add('fa-eye-slash');
                togglePassword2.classList.remove('fa-eye');
            }
        });
    </script>
</body>
</html>