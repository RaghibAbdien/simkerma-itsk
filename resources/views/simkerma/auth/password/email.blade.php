<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forget Password</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <div class="container reset">
        <div class="reset column-left">
            <div class="side-bar">
                <img src="assets/images/amico.png" alt="lupa password">
            </div>
        </div>
        <div class="reset column-right">
            <div class="button-back">
                <a href="{{ url('/login') }}" class="button" role="button"><i class="fas fa-arrow-left"></i>Kembali</a>
            </div>

            <div class="back-line">
                <a href="{{ url('/login') }}"><i class="fas fa-arrow-left"></i></a>
            </div>

            <h1>Lupa Kata Sandi?</h1>
            <label for="email">Alamat E-Mail</label>
            <div class="input-group">
                <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan alamat E-Mail" required>
            </div>
                
            <div class="button reset-button">
                <button type="submit">Kirim</button>
            </div>

            <p>Tidak Menerima E-mail? Kirim lagi setelah 00:15</p>
        </div>

        {{-- <div class="row">
            <div class="col-md-6">
                <div class="side-bar">
                    <img src="assets/images/amico.png" alt="lupa password">
                </div>
            </div>

            <div class="col-md-6">
                <div class="button-back">
                    <a href="{{ url('/login') }}" class="button" role="button"><i class="fas fa-arrow-left"></i>Kembali</a>
                </div>

                <div class="back-line">
                    <a href="{{ url('/login') }}"><i class="fas fa-arrow-left"></i></a>
                </div>

                <h1>Lupa Kata Sandi?</h1>
                    <form action="#" method="post" class="email-form">
                        <label for="email">Alamat E-Mail</label><br>
                        <input type="email" id="email" name="email" placeholder="Masukkan alamat E-Mail" required>
                
                        <div class="button email-button">
                            <button type="submit">Kirim</button>
                        </div>
                </form>

                <p>Tidak Menerima E-mail? Kirim lagi setelah 00:15</p>
            </div> --}}
        </div>
    </div>
</body>
</html>