<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <div class="container-login">
        <div class="square"></div>
        <div class="logo">
            <img src="assets/images/logo.jpg" width="250px" alt="logo">
        </div>
        
        <h1>Selamat Datang <br>di SIMKERMA</h1>
        <form action="#" method="post">
            <label for="email">Alamat E-Mail</label>
            <input type="email" id="email" name="email" placeholder="Masukkan alamat E-Mail" required>

            <label for="password">Kata Sandi</label>
            <div class="input-group">
                <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan kata sandi" required>
                <div class="input-group-append">
                  <span class="input-group-text">
                    <i class="fas fa-eye-slash" id="togglePassword"></i>
                  </span>
                </div>
            </div>
            <br><a href="{{ url('/resetpassword1') }}">Lupa kata sandi?</a><br>

            <div class="button login-button">
                <button type="submit">Masuk <i class="fas fa-arrow-right"></i></button>
            </div>
        </form>
    </div>

    <div class="image-container">
        <div class="square2"></div>
            <div class="logo2">
                <img src="assets/images/login.png" width="100%" alt="loginimg">
            </div>
    </div>

    <script>
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');

        togglePassword.addEventListener('click', function() {
            if (password.type === 'password') {
            password.type = 'text';
            togglePassword.classList.add('fa-eye');
            togglePassword.classList.remove('fa-eye-slash');
            } else {
            password.type = 'password';
            togglePassword.classList.add('fa-eye-slash');
            togglePassword.classList.remove('fa-eye');
            }
        });
    </script>
</body>
</html>