<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\SuperAdminController;
use App\Http\Controllers\ViewController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', [AuthController::class, 'showLogin']);
Route::get('/resetpassword1', [AuthController::class, 'showEmail']);
Route::get('/resetpassword2', [AuthController::class, 'showReset']);

Route::get('/hello', [ViewController::class, 'showHello']);

Route::get('/admin-dashboard', [AdminController::class, 'showAdmin']);

Route::get('/superadmin-dashboard', [SuperAdminController::class, 'showSuperAdmin']);

