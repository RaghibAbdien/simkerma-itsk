<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showLogin(){
        return view('simkerma/auth/login');
    }
    public function showEmail() {
        return view('simkerma/auth/password/email');
    }
    public function showReset() {
        return view('simkerma/auth/password/reset');
    }
}
