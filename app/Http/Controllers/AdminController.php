<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function showAdmin(){
        return view ('simkerma/admin/admin-dashboard');
    }
}
